package it.uninsubria.notescategories;

import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.Locale;

//classe che caratterizza ogni Noteitem
public class NoteItem {

    //attributi
    private String todo;
    private String categoria;
    private GregorianCalendar createOn;
    private GregorianCalendar deadline;
    private String createString;
    private String deadlineString;
    private int id;


    public NoteItem(String todo){
        super();
        id=0;
        this.todo = todo;
        this.createOn = new GregorianCalendar();
        this.deadline=new GregorianCalendar();
        this.categoria= "";
        createString=new SimpleDateFormat("dd/MM/yyyy", Locale.ITALIAN).format(createOn.getTime());
    }

    //getter & setter

    public byte[] image;

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public void setTodo(String todo) {
        this.todo = todo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public void setCreateOn(GregorianCalendar createOn) {
        this.createOn = createOn;
    }

    public void setDeadline(GregorianCalendar deadlinee) {
        this.deadline = deadlinee;
        deadlineString=new SimpleDateFormat("dd/MM/yyyy", Locale.ITALIAN).format(deadlinee.getTime());
    }

    public GregorianCalendar getDeadline() {
        return deadline;
    }

    public GregorianCalendar getCreateOn() {
        return createOn;
    }

    public String getCreateOnString() {  return createString; }

    public String getDeadLineString() {  return deadlineString; }

    public String getTodo() {
        return todo ;
    }

    @Override
    public String toString() {
        String ritorno= "Data Inizio: "+createString+" Data Fine: "+deadlineString+" TODO: "+todo+" Categoria: "+categoria;
        return ritorno;
    }

}
