package it.uninsubria.notescategories;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;

import java.util.HashMap;

//classe per generare il cerchio che mostra lo scadere della deadline

public final class CircleProgressBar extends View {

    private final RectF mDrawRect;
    private final Paint paint;
    private float percentage;
    private HashMap _$_findViewCache;

    public CircleProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mDrawRect = new RectF(0.0F, 0.0F, 0.0F, 0.0F);
        this.paint = new Paint();
        this.percentage = 0.75F;
    }

    public final float getPercentage() {
        return this.percentage;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public final void setPercentage(float value) {
        this.percentage = value;
        this.invalidate();
        this.requestLayout();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        this.paint.setStyle(Paint.Style.STROKE);
        this.paint.setAntiAlias(true);
        //int max_size = Integer.min(this.getWidth(), this.getHeight());
        int minimo;
        if(this.getWidth()<this.getHeight()){
            minimo=this.getWidth();
        }
        else{
            minimo=this.getHeight();
        }
        int max_size = minimo;
        this.paint.setStrokeWidth((float)max_size * 0.25F);
        float pad = this.paint.getStrokeWidth() * 0.6F;
        this.mDrawRect.set(0.0F + pad, 0.0F + pad, (float)this.getWidth() - pad, (float)this.getHeight() - pad);
        float startAngle = 0.0F;
        float drawTo = startAngle + this.getPercentage() * (float)360;
        this.paint.setColor(-3355444);
        canvas.drawArc(this.mDrawRect, 0.0F, 360.0F, false, this.paint);
        this.paint.setColor(ContextCompat.getColor(this.getContext(),R.color.colorPrimary));
        canvas.drawArc(this.mDrawRect, startAngle, drawTo, false, this.paint);
    }


}
