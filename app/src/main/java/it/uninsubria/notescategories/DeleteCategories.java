package it.uninsubria.notescategories;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class DeleteCategories extends AppCompatActivity {
    private Button confirmButton;
    private ListaElementi Elementi;
    private ArrayAdapter<String> spinnerAdapter;
    private String Category;
    DatabaseHelper mydb;
    Context deleteContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.delete_categories);

        //varaibili
        Elementi= ListaElementi.getInstance();
        mydb = new DatabaseHelper(this);
        confirmButton=findViewById(R.id.confirmDeleteCategory);
        deleteContext=this.getApplicationContext();
        spinnerAdapter=new ArrayAdapter<String>(this, R.layout.riga);

        //toolbar
        Toolbar myToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar); //settata come toolbar principale
        getSupportActionBar().setLogo(R.mipmap.ic_launcher);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true); //visualizza la freccia
        getSupportActionBar().setDisplayShowHomeEnabled(true); //abilita la freccia
        getSupportActionBar().setTitle("Delete category");


        //spinner
        spinnerAdapter.addAll(Elementi.getElemento());
        final Spinner sp=(Spinner) findViewById(R.id.Elementi);
        sp.setAdapter(spinnerAdapter);

        sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Category = sp.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Elementi.removeElemento(Category);
                Intent resultIntent = new Intent();
                resultIntent.putExtra("CATEGORY",Category);
                setResult(RESULT_OK, resultIntent); //passa intent e RESULT_OK al main (codice DELETE_CATEGORIES_REQUEST)
                finish();
            }
        });

    }

    // se viene premuta la freccia ritorna alla main activity senza eliminare la categoria
    public boolean onSupportNavigateUp() {
        Intent resultIntent = new Intent(this,MainActivity.class);
        startActivity(resultIntent);
        return true;
    }


}
