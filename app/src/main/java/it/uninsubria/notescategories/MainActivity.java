package it.uninsubria.notescategories;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;

/*
Iannello Stefano
Matricola: 736895

----Riepilogo----

Consegna:

Applicazione che mantiene una lista di note suddivise in categorie. Le categorie sono configurate dall'utente e possono essere aggiunte ed eliminate.

Ogni nota potrà contenere testo scritto con l’applicazione e/o foto del testo scritto su supporto cartaceo.

L’applicazione dovrà implementare delle gesture (es . eliminazione rapida nota) per le azioni più comuni.
----------------------------------------------------------------------------------------------------------------------------------------------------
Realizzazione:

Ho realizzato un'applicazione che permette di aggiungere, eliminare, modificare, visualizzare delle note.
ogni nota contiene del testo e/o una fotografia scattata dalla camera del dispositivo.
ad ogni nota viene associata una categoria che puo' essere aggiunta o eliminata dall'utente.

ho voluto utilizzare una visualizzazzione stile "to do list" per il menu principale, che mostra solo le note della categoria scelta dal menu' a tendina
infatti ad ogni nota è possibile inserire una data di deadline nel caso se ne avesse la necessità
oppure lasciarla vuota. viene visualizzata anche la progress bar in base al giorno e la deadline, se inserita.

modifica nota: ad una nota già inserita, puo' essere modificato il testo, la categoria e l'immagine scattata tramite fotocamera

controllo su duplicazione categorie --> non possono essere aggiunte categorie duplicate
controllo su eliminazione categorie --> se viene eliminata una categoria associata a delle note, le note ottengono la categoria "All" (default)
controllo su messaggio nota --> non possono essere aggiunte note senza testo (possono essere aggiunte note senza immagine)

Gestures:

Eliminazione rapida nota: effettuare uno swipe da sinistra verso destra sulla nota che si intende eliminare

Visualizzazione nota full-screen: effettuare uno swipe da destra a sinistra sulla nota che si intende visualizzare

Modifica nota: effettuare un tap prolungato sulla nota che si intende modificare

*/

public class MainActivity extends AppCompatActivity {
    //varaibili
    static final int DELETE_CATEGORIES_REQUEST = 3;
    static final int ADD_CATEGORIES_REQUEST = 2;
    static final int ADD_NOTES_REQUEST = 1;
    ArrayList<NoteItem> toDoItems;
    private Button deleteButton;
    private ArrayAdapter<String> listViewAdapter;
    private ArrayAdapter<String> spinnerAdapter;
    TodoArrayAdapter adapter;
    static DatabaseHelper mydb;
    private ListaElementi listaelementi;
    private Context mainContext;
    private static final String TAG = "MainActivity"; //creare i log
    private String Category;
    String ok="";
    Cursor result;
    Spinner sp;
    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //toolbar
        Toolbar myToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setLogo(R.mipmap.ic_launcher);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        mydb = new DatabaseHelper(this); //chiama il costruttore della classe DatabaseHelper (crea db e table)
        mainContext=this.getApplicationContext();


        final ListView listView = findViewById(R.id.listView);
        deleteButton = findViewById(R.id.deleteButton);

        toDoItems = new ArrayList<NoteItem>(); //arraylist che contiene i todoItem

        adapter = new TodoArrayAdapter(MainActivity.this, R.layout.list_item, toDoItems);
        ListView lv=(ListView) findViewById(R.id.valorimostra);

        //spinner e set adapter listviews
        sp=(Spinner) findViewById(R.id.spinnermostra);
        spinnerAdapter=new ArrayAdapter<String>(this, R.layout.riga);
        spinnerAdapter.clear();
        spinnerAdapter.notifyDataSetChanged();

        listViewAdapter=new ArrayAdapter<String>(this,R.layout.riga);


        lv.setAdapter(listViewAdapter);
        listView.setAdapter(adapter);

        listaelementi = ListaElementi.getInstance();
        listaelementi.refreshCategories(mydb);
        adapter.notifyDataSetChanged();

        spinnerAdapter.clear();
        spinnerAdapter.addAll(listaelementi.getElemento());
        spinnerAdapter.notifyDataSetChanged();
        sp.setAdapter(spinnerAdapter);



        //quando viene selezionato un item dallo spinner
        sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                toDoItems.clear();
                Category=sp.getSelectedItem().toString();

                    if(Category.equals("All")){ //se viene selezionata la categoria "All" vengono mostrate tutte le note
                        result = mydb.getAllData();
                    }
                    else {
                        result = mydb.getDataFromCategory(Category); //altrimenti vengono mostrate solo le note di quella categoria
                    }

                    if (result.getCount() <= 0) {
                        toDoItems.clear();
                        adapter.notifyDataSetChanged();

                    } else {

                        while (result.moveToNext()) {
                            //setta le variabili dal cursore
                            int id2 = result.getInt(0);
                            String deadline = result.getString(3);
                            String createline = result.getString(2);
                            String todo = result.getString(1);
                            String Category = result.getString(4);
                            byte[] Image = result.getBlob(5);

                            //conversione date
                            DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                            Date date = null;
                            Date date2 = null;
                            try {
                                date = format.parse(createline);
                                date2 = format.parse(deadline);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            GregorianCalendar calendar = new GregorianCalendar();
                            GregorianCalendar calendar2 = new GregorianCalendar();
                            calendar.setTime(date);
                            calendar2.setTime(date2);

                            //viene creata una nota temporanea con i dati acquisiti
                            NoteItem dbTodo = new NoteItem(result.getString(1));
                            dbTodo.setCreateOn(calendar);
                            dbTodo.setDeadline(calendar2);
                            dbTodo.setCategoria(Category);
                            dbTodo.setImage(Image);
                            //viene aggiunta all'array di note
                            toDoItems.add(0, dbTodo);
                            adapter.notifyDataSetChanged();

                        }
                    }
                }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //bottone che elimina tutte le note
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mydb.deleteAll();
                toDoItems.clear();
                adapter.notifyDataSetChanged();

            }
        });


        //Gesture longclick: modifica la nota
        registerForContextMenu(listView);
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int pos, long id) {

                int idt= toDoItems.get(pos).getId();
                String todo=toDoItems.get(pos).getTodo();
                String categoria= toDoItems.get(pos).getCategoria();
                Intent modifyIntent= new Intent(mainContext,ModifyNote.class);
                modifyIntent.putExtra("ID", idt );
                modifyIntent.putExtra("CATEGORIA", categoria );
                modifyIntent.putExtra("TODO", todo );
                startActivity(modifyIntent);
                return true;
            }
        });

        final SwipeDetector swipeDetector=new SwipeDetector();
        listView.setOnTouchListener(swipeDetector);

       //Gesture: Swipe da sinistra a destra: eliminazione rapida nota
        AdapterView.OnItemClickListener listener = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {
                if (swipeDetector.swipeDetected()) {
                    if (swipeDetector.getAction() == SwipeDetector.Action.LR) {

                        String todo= toDoItems.get(pos).getTodo();
                        String createon= toDoItems.get(pos).getCreateOnString();
                        String deadline= toDoItems.get(pos).getDeadLineString();
                        String category= toDoItems.get(pos).getCategoria();
                        mydb.deletePos(todo,createon,deadline,category);
                        refresh();
                    }
                    else{

        //Gesture: Swipe da destra a sinistra visualizzazione nota full screen
                        Log.d("swipe","da destra a sinistra");
                        String todo=toDoItems.get(pos).getTodo();
                        byte[] image= toDoItems.get(pos).getImage();
                        Intent modifyIntent= new Intent(mainContext,VisualizeNote.class);
                        modifyIntent.putExtra("IMAGE", image );
                        modifyIntent.putExtra("TODO", todo );
                        startActivity(modifyIntent);
                    }
                }
            }
        };
        listView.setOnItemClickListener(listener);

    }


    //refresh
    public void refresh(){
        this.recreate();
    }


    //settaggio toolbar (inflate)
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_activity_menu, menu);
        return true;
    }

    //handler onOptionItemSelected (metodo invocato dal listener associato alle actionbar)
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_add_notes) {  //se viene premuta l'icona per aggiungere una nuova nota
            Intent i = new Intent(getApplicationContext(), AddNotes.class);
            startActivityForResult(i, ADD_NOTES_REQUEST); //costante identificiativa dell'activity per identificare il valore di ritorno
            return true;

        } else if (id == R.id.action_add_categories) {     //se viene premuta l'icona per aggiungere una nuova categoria
            Intent i = new Intent(getApplicationContext(), AddCategories.class);
            startActivityForResult(i, ADD_CATEGORIES_REQUEST);
            return true;

        } else if (id == R.id.action_exit) { //se viene premuta l'icona per uscire dall'app (non viene killata)
            Intent homeIntent = new Intent(Intent.ACTION_MAIN);
            homeIntent.addCategory( Intent.CATEGORY_HOME );
            homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(homeIntent);
        }
        else if (id == R.id.action_delete_categories) { //se viene premuta l'icona per aggiungere una eliminare una categoria
            Intent i = new Intent(this,DeleteCategories.class);
            startActivityForResult(i, DELETE_CATEGORIES_REQUEST);
            return true;

        }
        else if (id == R.id.action_info) { //se viene premuta l'icona per mostrare le info
            Intent i = new Intent(this,InfoVisualizer.class);
            startActivity(i);
            return true;
        }
        else if(id==R.id.action_refresh){ //se viene premuta l'icona per fare il refresh
            refresh();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ADD_NOTES_REQUEST) { //se si è cliccato su aggiungere una nuova nota

            if (resultCode == RESULT_OK) { //se l'activity è stata terminata corretamente
                String returnValue = data.getStringExtra("TODO_TASK");
                String returnValue2 = data.getStringExtra("DEADLINE");
                String category = data.getStringExtra("CATEGORY");
                byte[] image = data.getByteArrayExtra("IMAGE");

                Log.d(MainActivity.class.getName(), "onActivityResult() -> " + returnValue);

                if (returnValue != null) {
                    DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                    Date date = null;
                    try {
                        date = format.parse(returnValue2);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    GregorianCalendar calendar = new GregorianCalendar();
                    calendar.setTime(date);

                    //aggiunge la nuova nota tramite il metodo addNewItem
                    addNewItem(returnValue, calendar, category, image);
                    adapter.notifyDataSetChanged();
                    refresh();

                }
            }
        }

        if (requestCode == ADD_CATEGORIES_REQUEST) { //se si è cliccato su aggiungere una nuova categoria

            if (resultCode == RESULT_OK) {//se l'activity è stata terminata corretamente
                ok = data.getStringExtra("OK"); //se la categoria non era duplicata
                if (ok.equals("true")) {
                    String category = data.getStringExtra("CATEGORY"); //legge il valore di ritorno dentro l'intent

                    //aggiunge la nuova categoria tramite il metodo addNewCategory
                    addNewCategory(category);
                    refresh();
                }
            } else {

            }
            adapter.notifyDataSetChanged();
        }


        if (requestCode == DELETE_CATEGORIES_REQUEST) { //se si è cliccato su eliminare una categoria

            if (resultCode == RESULT_OK) {
                String Category= data.getStringExtra("CATEGORY");
                if(Category.equals("All")){
                    Toast.makeText(getApplicationContext(),
                            "Can't delete 'All' category", Toast.LENGTH_LONG).show();
                }
                else {
                    deleteCategory(Category); //elimina la categoria tramite il metodo deleteCategory
                }

            }
            adapter.notifyDataSetChanged();
        }

    }

    //metodo per aggiungere una nuova nota
    private void addNewItem(String todo,GregorianCalendar deadline,String categoria,byte[] image) {

        if (todo.length() == 0) {
            Toast.makeText(getApplicationContext(),"Empty Note string", Toast.LENGTH_LONG).show();
            return;
        }

        //Crea una nuova nota temporanea se la nota contiene del testo
        NoteItem newTodo = new NoteItem(todo);
        newTodo.setDeadline(deadline);
        newTodo.setCategoria(categoria);
        newTodo.setImage(image);
        toDoItems.add(0,newTodo);

        boolean inserito = mydb.insertData(newTodo); //inserisce la nota nel db, ritorna true se inserito con successo, false altrimenti

        if (inserito == true) {
            Toast.makeText(MainActivity.this, "Nota inserita", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(MainActivity.this, "ERRORE non inserito nel DB", Toast.LENGTH_LONG).show();
        }
        adapter.notifyDataSetChanged();
    }

    //metodo per aggiungere una nuova category
    private void addNewCategory(String category) {

        boolean inserito = mydb.insertCategory(category); //inserisce la categoria nel db, ritorna true se inserito con successo, false altrimenti

        if (inserito == true) {
             //Toast.makeText(MainActivity.this, "Categoria inserita", Toast.LENGTH_LONG).show();
        } else {
             //Toast.makeText(MainActivity.this, "ERRORE non inserito nel DB", Toast.LENGTH_LONG).show();
        }

    }

    //metodo per eliminare una category
    private void deleteCategory(String category) {

        if(!category.equals("All")) {
            mydb.deleteCategory(category); //elimina la category dal db
            listaelementi.removeElemento(category);
        }

        refresh();
        spinnerAdapter.notifyDataSetChanged();
        adapter.notifyDataSetChanged();
        Toast.makeText(mainContext, "Category: "+Category+" deleted", Toast.LENGTH_LONG).show();
    }

    //ritorna l'istanza del db
    public static DatabaseHelper getMyDb(){
        return mydb;
    }

}



