package it.uninsubria.notescategories;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class VisualizeNote extends AppCompatActivity { //classe utilizzata per visualizzare immagine e testo di una nota

    //variabili
    ImageView imageView;
    TextView textView;
    String todo;
    byte[] imagearray;
    Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.visualize_note);

        //toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar); //inserisce la toolbar nell'activity
        getSupportActionBar().setDisplayHomeAsUpEnabled(true); //visualizza la freccia
        getSupportActionBar().setDisplayShowHomeEnabled(true); //abilita la freccia
        getSupportActionBar().setTitle("Visualize note");

        //dichiarazione & set views
        imageView=findViewById(R.id.noteImage);
        textView=findViewById(R.id.noteText);
        todo= getIntent().getExtras().getString("TODO");
        imagearray= getIntent().getByteArrayExtra("IMAGE");
        if(imagearray!=null) {
            bitmap = BitmapFactory.decodeByteArray(imagearray, 0, imagearray.length);
            imageView.setImageBitmap(bitmap);
        }
        textView.setText(todo);

    }

    // se viene premuta la freccia ritorna alla main activity
    public boolean onSupportNavigateUp() {
        Intent resultIntent = new Intent(this,MainActivity.class);
        startActivity(resultIntent);
        return true;
    }
}
