package it.uninsubria.notescategories;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class InfoVisualizer extends AppCompatActivity { //classe per info su di me
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.info_layout);

        //toolbar
        Toolbar myToolbar = findViewById(R.id.toolbar); //Cast
        setSupportActionBar(myToolbar); //settata come toolbar principale
        getSupportActionBar().setDisplayHomeAsUpEnabled(true); //visualizza la freccia
        getSupportActionBar().setDisplayShowHomeEnabled(true); //abilita la freccia
        getSupportActionBar().setTitle("About me...");
    }

    // se viene premuta la freccia ritorna alla main activity
    public boolean onSupportNavigateUp() {
        Intent resultIntent = new Intent(this,MainActivity.class);
        startActivity(resultIntent);
        return true;
    }
}
