package it.uninsubria.notescategories;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.io.ByteArrayOutputStream;
import java.util.Calendar;

public class AddNotes extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    private ListView category;
    private TextView deadLine;
    private EditText editText;
    private Button confirmButton;
    private ListaElementi Elementi;
    private ArrayAdapter<String> listViewAdapter;
    private ArrayAdapter<String> spinnerAdapter;
    private ImageView imageview;
    private String Category;
    private Button openCamera;
    byte[] imagedata;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_notes);

        //variabili
        imageview=findViewById(R.id.add_notes_imageView);
        Toolbar toolbar = findViewById(R.id.toolbar);
        deadLine = findViewById(R.id.addDeadLineTextView);
        category=findViewById(R.id.valoriElementi);
        confirmButton=findViewById(R.id.confirmNewNote);
        openCamera=findViewById(R.id.openCamera);
        editText= findViewById(R.id.editNewNote);

        //toolbar
        setSupportActionBar(toolbar); //inserisce la toolbar nell'activity
        getSupportActionBar().setDisplayHomeAsUpEnabled(true); //visualizza la freccia
        getSupportActionBar().setDisplayShowHomeEnabled(true); //abilita la freccia
        getSupportActionBar().setTitle("Add New Note");

        //permessi camera
        if(ContextCompat.checkSelfPermission(AddNotes.this,Manifest.permission.CAMERA)!=
        PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(AddNotes.this,new String[]{
                    Manifest.permission.CAMERA
            },100);
        }
        openCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent,100);
            }
        });


        //preparazione listview per elenco elementi
        ListView lv=(ListView) findViewById(R.id.valoriElementi);
        listViewAdapter=new ArrayAdapter<String>(this,R.layout.riga);
        lv.setAdapter(listViewAdapter);
        Elementi= ListaElementi.getInstance();

        //preparazione dello spinner
        spinnerAdapter=new ArrayAdapter<String>(this, R.layout.riga);
        Log.d("key",Elementi.getElemento().toString());
        spinnerAdapter.addAll(Elementi.getElemento());
        final Spinner sp=(Spinner) findViewById(R.id.Elementi);
        sp.setAdapter(spinnerAdapter);
        sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Category = sp.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        deadLine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerDialog();
            }
        });

        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(deadLine.getText()==""){
                    deadLine.setText("1/1/2080");
                }

                Intent resultIntent = new Intent();
                resultIntent.putExtra("TODO_TASK", editText.getText().toString());
                resultIntent.putExtra("DEADLINE", deadLine.getText().toString());
                resultIntent.putExtra("CATEGORY",Category);
                resultIntent.putExtra("IMAGE", imagedata);
                setResult(RESULT_OK, resultIntent); //passa intent e RESULT_OK al main (codice ADD_NOTES_REQUEST)
                finish();
            }
        });
    } //onCreate


    @Override
    protected void onActivityResult(int requestCode,int resultCode,Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==100){
            //cattura immagine
            Bitmap captureImage=(Bitmap) data.getExtras().get("data");
            //setta immagine nella imageview
            imageview.setImageBitmap(captureImage);
            //Convert to byte array
            ByteArrayOutputStream baos=new ByteArrayOutputStream();
            captureImage.compress(Bitmap.CompressFormat.JPEG,100,baos);
            imagedata=baos.toByteArray();
        }
    }

    private void showDatePickerDialog(){
        DatePickerDialog datePickerDialog= new DatePickerDialog(
                this,
                this,
                Calendar.getInstance().get(Calendar.DAY_OF_MONTH),
                Calendar.getInstance().get(Calendar.MONTH),
                Calendar.getInstance().get(Calendar.YEAR)
        );
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.show();
    }

    // se viene premuta la freccia ritorna alla main activity senza aggiungere la nota
    @Override
    public boolean onSupportNavigateUp() {
        Toast.makeText(this, "Note not added", Toast.LENGTH_LONG).show();
        Intent resultIntent = new Intent(this,MainActivity.class);
        startActivity(resultIntent);
        return true;
    }

    //set della eventuale deadline tramite il calendario
    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        month++;
        String date= dayOfMonth+"/"+month+"/"+year;
        deadLine.setText(date);
    }

}
