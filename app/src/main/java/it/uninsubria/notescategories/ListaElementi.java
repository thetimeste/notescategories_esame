package it.uninsubria.notescategories;

import android.database.Cursor;
import android.util.Log;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Set;

public class ListaElementi { //classe singleton per lista di elementi degli spinner

    //variabili
    boolean found; //per identificare se l'elemento da rimuovere è presente nella hashMap
    private HashMap <String, ArrayList<String>> list=new HashMap<String, ArrayList<String>>();

    //pattern singleton
    private static ListaElementi instance = new ListaElementi();

    public ListaElementi(){ //All deve sempre essere presente nella lista di elementi
        addElemento("All" );
    }

    public static ListaElementi getInstance(){
        return instance;
    }

    //per aggiungere tutti gli elementi nel database alla hashMap
    public  void refreshCategories(DatabaseHelper mydb){
        Cursor result = mydb.getAllCategories();
        if (result.getCount() <= 0) {

        } else {
            while (result.moveToNext()) {
                String category = result.getString(1);
                addElemento(category);
            }
        }
    }

    //controlla ed aggiunge l'elemento passato alla Hashmap se non è duplicato. se è duplicato temp=false, true altrimenti
    public boolean addElemento(String elemento){
        Set<String> keySet = list.keySet();
        boolean temp=true;
        for(String key:keySet){
            if(key.equals(elemento)){
                temp=false;
            }
        }

        if(temp==true){
            list.put(elemento,null);
            //ritorna true se l'elemento è stato inserito, false se era gia' presente
            return true;
        }
        if(temp==false){
            return false;
        }
        return false;
    }

    //rimuove l'elemento dato, se presente found=true, false altrimenti --> per evitare eccezione concurrentmodify
    public void removeElemento(String elemento){

        Set<String> keySet = list.keySet();
        for(String key:keySet){
            if(key.equals(elemento)){
                found=true;
            }

        }

        //se l'elemento è "All" non viene eliminato
        if(elemento.equals("All")){
            found=false;
        }

        //rimozione elemento
        if(found==true){
            found=false;
            list.remove(elemento);
        }
    }

    //ritorna il keyset della HaspMap
    public Collection<String> getElemento(){
        return list.keySet();
    }

}
