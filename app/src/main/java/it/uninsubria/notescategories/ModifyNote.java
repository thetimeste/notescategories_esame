package it.uninsubria.notescategories;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import java.io.ByteArrayOutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;


//classe che permette la modifica delle note
public class ModifyNote extends AppCompatActivity {

    //variabili
    DatabaseHelper mydb;
    EditText modifyEditText;
    ImageView modifyImageView;
    Button confirmButton;
    Button modifyPhoto;
    int id; //id ricevuto dall'intent
    static int id2; //id ricevuto dal DB
    String todo;
    ListaElementi Elementi;
    String categoria;
    Context modifyContext;
    Bitmap bitmap;
    byte[] imagedata;
    private ArrayAdapter<String> listViewAdapter;
    private ArrayAdapter<String> spinnerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.modify_note);

        //istanziazione variabili
        modifyContext=getApplicationContext();
        id= getIntent().getExtras().getInt("ID");
        categoria= getIntent().getExtras().getString("CATEGORIA");
        todo= getIntent().getExtras().getString("TODO");
        mydb=MainActivity.getMyDb(); //prende l'istanza del DB dalla mainactivity
        modifyEditText=findViewById(R.id.modify_note_editText);
        modifyImageView=findViewById(R.id.modify_note_imageView);
        confirmButton=findViewById(R.id.confirmModify);
        modifyPhoto=findViewById(R.id.modifyPhoto);
        ListView lv=(ListView) findViewById(R.id.ElementiModifyListView);
        listViewAdapter=new ArrayAdapter<String>(this,R.layout.riga);
        lv.setAdapter(listViewAdapter);
        Elementi= ListaElementi.getInstance();

        //richiama metodo per visualizzare informazioni della nota
        displayNote(id,categoria,todo,mydb);

        //Toolbar
        Toolbar myToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setLogo(R.mipmap.ic_launcher);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Modify note");

        //preparazione dello spinners
        spinnerAdapter=new ArrayAdapter<String>(this, R.layout.riga);
        spinnerAdapter.addAll(Elementi.getElemento());
        final Spinner sp=(Spinner) findViewById(R.id.ElementiModifySpinner);
        sp.setAdapter(spinnerAdapter);
        sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    categoria= sp.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //vengono inviati tutti gli aggiornamenti al database
                mydb.updateNote(id2,modifyEditText.getText().toString());
                mydb.updateImage(id2,imagedata);
                mydb.updateCategory(id2,categoria);

                //ritorna alla main activity
                Intent returnMain=new Intent(modifyContext,MainActivity.class);
                startActivity(returnMain);
                Toast.makeText(modifyContext, "Note successfully modified", Toast.LENGTH_LONG).show();
            }
        });
        modifyPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //attiva la fotocamera
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent,100);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode,int resultCode,Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==100){ //se è stata scattata la foto

            //cattura immagine
            Bitmap captureImage=(Bitmap) data.getExtras().get("data");

            //setta immagine nella imageview
            modifyImageView.setImageBitmap(captureImage);

            //Converte to byte array
            ByteArrayOutputStream baos=new ByteArrayOutputStream();
            captureImage.compress(Bitmap.CompressFormat.JPEG,100,baos);
            imagedata=baos.toByteArray();
        }
    }

    // se viene premuta la freccia ritorna alla main activity senza modificare la nota
    public boolean onSupportNavigateUp() {
        Toast.makeText(this, "Note not modified", Toast.LENGTH_LONG).show();
        Intent resultIntent = new Intent(this,MainActivity.class);
        startActivity(resultIntent);
        return true;
    }


    public void displayNote(int id,String categoria,String todo,DatabaseHelper mydb2) {

        //riceve il cursore
        Cursor result = mydb2.getDataFromCN(categoria,todo);

        if (result.getCount() <= 0) {

        } else {

            while (result.moveToNext()) {
                //setta le variabili dal cursore
                String deadline = result.getString(3);
                String createline = result.getString(2);
                String todo2 = result.getString(1);
                String Category = result.getString(4);
                byte[] image=result.getBlob(5);
                id2= result.getInt(0);

                //setta la bitmap se l'immagine non è nulla
                if(image!=null) {
                    bitmap = BitmapFactory.decodeByteArray(image, 0, image.length);
                }

                //conversione date
                DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                Date date = null;
                Date date2 = null;
                try {
                    date = format.parse(createline);
                    date2 = format.parse(deadline);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                GregorianCalendar calendar = new GregorianCalendar();
                GregorianCalendar calendar2 = new GregorianCalendar();
                calendar.setTime(date);
                calendar2.setTime(date2);

                //setta imageview ed edittext
                modifyImageView.setImageBitmap(bitmap);
                modifyEditText.setText(todo);

            }
        }
    }


}
