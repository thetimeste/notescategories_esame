package it.uninsubria.notescategories;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import java.util.ArrayList;
import java.util.GregorianCalendar;

public class TodoArrayAdapter<image> extends ArrayAdapter<NoteItem> {
    private Context mContext;

    public TodoArrayAdapter(Context context, int resource, ArrayList<NoteItem> toDoItems) {
        super(context,R.layout.list_item, toDoItems);
        this.mContext = context;

    }

    //setta e ritorna la convertview
    @RequiresApi(api = Build.VERSION_CODES.N)
    @NonNull
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        //varaibili
        final String todo=getItem(position).getTodo();
        final String deadline=getItem(position).getDeadLineString();
        final String deadlinetemp="deadline not inserted";
        final String start=getItem(position).getCreateOnString();


        if(convertView==null) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(R.layout.list_item, parent, false);
        }

        //dichiarazione views
        final TextView tvCategory= convertView.findViewById(R.id.categoryTextView);
        final TextView tvTodo= convertView.findViewById(R.id.messageTextView);
        final TextView tvStart= convertView.findViewById(R.id.createdTextView);
        final TextView tvEnd= convertView.findViewById(R.id.deadlineTextView);
        final ImageView img= convertView.findViewById(R.id.imageView);
        final CircleProgressBar circle= convertView.findViewById(R.id.circleProgressBar);
        final TextView tvcircle=convertView.findViewById(R.id.circle);

        img.setImageResource(R.mipmap.todo_listview_icon_foreground);


        //generazione cerchio scadenza deadline
        float created= getItem(position).getCreateOn().getTimeInMillis();
        float dead= getItem(position).getDeadline().getTimeInMillis();
        GregorianCalendar nowCal=new GregorianCalendar();
        float time=nowCal.getTimeInMillis();
        float deltaMillis=time-created;
        float intervalMillis= dead-created;
        float valore;
        valore= deltaMillis/intervalMillis;
        float valore2=valore;

        circle.setPercentage(valore2);
        String circlevalue= String.valueOf(valore2);
        String category= getItem(position).getCategoria();

        //set delle views
        tvcircle.setText(circlevalue);
        tvTodo.setText(todo);
        tvStart.setText(start);
        tvEnd.setText(deadline);
        tvCategory.setText(category);

        // se non è stata scelta una deadline
        if(tvEnd.getText().equals("01/01/2080")){
            tvEnd.setText(deadlinetemp);
        }
        //altrimenti
        else {
            tvEnd.setText(deadline);
        }
        return convertView;
    }
}