package it.uninsubria.notescategories;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class AddCategories extends AppCompatActivity {
    private Button confirmCategoriesButton;
    private EditText categoriesEditText;
    private ListaElementi listaelementi;
    private Context AddCategoriesContext;
    private String ok; //per indicare se l'inserimento nella lista di elementi è andato a buon fine
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AddCategoriesContext=this.getApplicationContext();
        listaelementi= ListaElementi.getInstance();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_categories);
        confirmCategoriesButton=(Button) findViewById(R.id.confirmNewCategory);
        categoriesEditText=(EditText) findViewById(R.id.addCategoryEditText);

        //Toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar); //inserisce la toolbar nell'activity
        getSupportActionBar().setDisplayHomeAsUpEnabled(true); //visualizza la freccia
        getSupportActionBar().setDisplayShowHomeEnabled(true); //abilita la freccia
        getSupportActionBar().setTitle("Add New Category");
        ok="false";
        confirmCategoriesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!categoriesEditText.getText().toString().trim().equals("")) {  //se la categoria non è vuota
                    addCategory(categoriesEditText.getText().toString()); //prova ad aggiungerla alla lista di categorie
                    Intent resultIntent = new Intent();
                    resultIntent.putExtra("CATEGORY", categoriesEditText.getText().toString());
                    resultIntent.putExtra("OK", ok);
                    setResult(RESULT_OK, resultIntent); //passa intent e RESULT_OK al main (codice ADD_CATEGORIES_REQUEST)
                    finish();
                }
                else{
                    Toast.makeText(AddCategoriesContext, "Empty category", Toast.LENGTH_LONG).show();
                }
            }
        });
    }


    //controlla se la categoria inserita è un duplicato, se è duplicato ok=false, se non è duplicato ok=true.
   public boolean addCategory(String category){

       listaelementi= ListaElementi.getInstance();

       boolean risultato= listaelementi.addElemento(category);
       if(risultato==true){
           Toast.makeText(this, "Category: "+category+" added successfully", Toast.LENGTH_LONG).show();
           ok="true";
           return true;
       }
       else{
           Toast.makeText(this, "Category: "+category+" is already registered", Toast.LENGTH_LONG).show();
           ok="false";
           return false;
       }

   }

   // se viene premuta la freccia ritorna alla main activity senza aggiungere la categoria
    @Override
    public boolean onSupportNavigateUp() {
        Toast.makeText(this, "Category not added", Toast.LENGTH_LONG).show();
        Intent resultIntent = new Intent(this,MainActivity.class);
        startActivity(resultIntent);
        return true;
    }
}
