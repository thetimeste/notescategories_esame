package it.uninsubria.notescategories;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME= "notes.db"; //nome del database
    public static final String TABLE_NAME = "note_table"; //nome della table per le note
    public static final String TABLE_NAME2 = "categories_table"; //nome della table per le categorie
    public static final String ID ="id";        //campo della table
    public static final String MEX = "MEX"; //campo della table
    public static final String DAT = "DAT"; //campo della table
    public static final String DEADLINE = "DEADLINE"; //campo della table
    public static final String CATEGORY="CATEGORY"; //campo della table
    public static final String IMAGE="IMAGE"; //campo della table

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 2); //serve per creare il database vero e proprio
    }


    @Override
    public void onCreate(SQLiteDatabase db) { //per creare le table

        db.execSQL("CREATE TABLE "+TABLE_NAME+" (ID INTEGER PRIMARY KEY AUTOINCREMENT, MEX VARCHAR(20) NOT NULL, DAT VARCHAR(20),DEADLINE VARCHAR(20),CATEGORY TEXT,IMAGE BLOB)");
        db.execSQL("CREATE TABLE "+TABLE_NAME2+" (ID INTEGER PRIMARY KEY AUTOINCREMENT, CATEGORY VARCHAR(20))");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME2);
        onCreate(db);
    }

    public boolean insertData(NoteItem item){ //inserisce una nuova nota nella table (note_table) ritorna true se andato a buon fine, false altrimenti
        SQLiteDatabase db= this.getWritableDatabase();
        ContentValues contentValues= new ContentValues();
        contentValues.put(MEX,item.getTodo());
        String dat=item.getCreateOnString();
        contentValues.put(DAT,dat);
        String ded=item.getDeadLineString();
        contentValues.put(DEADLINE,ded);
        contentValues.put(CATEGORY,item.getCategoria());
        contentValues.put(IMAGE,item.getImage());

        long result= db.insert(TABLE_NAME,null,contentValues); //se errore ritorna -1. se ok ritorna la riga

        if(result==-1){
            return false;
        }
        else{
            return true;
        }
    }

    public boolean insertCategory(String category){  //inserisce una nuova categoria nella table (categories_table) ritorna true se andato a buon fine, false altrimenti
        SQLiteDatabase db= this.getWritableDatabase(); //crea database e table
        ContentValues contentValues= new ContentValues();
        contentValues.put(CATEGORY,category);

        long result= db.insert(TABLE_NAME2,null,contentValues);

        if(result==-1){
            return false;
        }
        else{
            return true;
        }
    }

    public void deleteAll(){ //elimina la table note_table
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME,null,null);
        db.execSQL("delete from "+ TABLE_NAME);
    }

    public void deleteCategory(String category){ //elimina la categoria passata dalla table categories_table
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("UPDATE "+ TABLE_NAME+ " SET "+CATEGORY+"='"+"All"+"' WHERE "+CATEGORY+ "='"+category+"'");
        db.execSQL("DELETE FROM "+ TABLE_NAME2+ " WHERE "+CATEGORY+"='"+category+"'");
    }

    public Cursor getAllData(){  //ritorna tutti i record della table note_table
        SQLiteDatabase db= this.getWritableDatabase();
        Cursor result=db.rawQuery("SELECT * FROM "+TABLE_NAME,null);
        return result;
    }
    public Cursor getAllCategories(){ //ritorna tutte le categorie della table categories_table
        SQLiteDatabase db= this.getWritableDatabase();
        Cursor result=db.rawQuery("SELECT * FROM "+TABLE_NAME2,null);
        return result;
    }

    public Cursor getDataFromCN(String categoria,String todo){ //ritorna tutti i record con categoria e note dati
        SQLiteDatabase db= this.getWritableDatabase();
        Cursor result2=db.rawQuery("SELECT * FROM "+TABLE_NAME+" WHERE "+MEX+ "='"+todo+"'"+" AND "+CATEGORY+"='"+categoria+"'",null);
        return result2;
    }
    public Cursor getDataFromCategory(String categoria){ //data una categoria ritorna tutte le note con quella categoria
        SQLiteDatabase db= this.getWritableDatabase();
        Cursor result2=db.rawQuery("SELECT * FROM "+TABLE_NAME+" WHERE "+CATEGORY+"='"+categoria+"'",null);
        return result2;
    }

    public void updateNote(int id,String messaggio){ //cambia il messaggio della nota dato l'id della nota e il nuovo messaggio
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("UPDATE "+ TABLE_NAME+ " SET "+MEX+"='"+messaggio+"' WHERE "+ID+ "='"+id+"'");
        Log.d("querymodifica","query di modifica nota eseguita:");

    }
    public void updateImage(int id,byte[] image){  //cambia l'immagine della nota dato l'id e la nuova immagine
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("image", image);
        db.update(TABLE_NAME, cv, "ID="+id, null);
        Log.d("querymodifica","query di modifica image eseguita:");

    }
    public void updateCategory(int id,String category){ //cambia la categoria della nota dato l'id e la nuova categoria
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("UPDATE "+ TABLE_NAME+ " SET "+CATEGORY+"='"+category+"' WHERE "+ID+ "='"+id+"'");
        Log.d("querymodifica","query di modifica image eseguita:");

    }
    public void deletePos(String todo,String createon,String deadline,String category){ //elimina una nota dato il messaggio,data creazione,deadline e categoria
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM "+ TABLE_NAME + " WHERE "+MEX+ "='"+todo+"'"+" AND "+DEADLINE+"='"+deadline+"'"+" AND "+DAT+"='"+createon+"' AND "+CATEGORY+"='"+category+"'");
    }
}
